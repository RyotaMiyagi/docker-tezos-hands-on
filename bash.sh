#!/bin/sh

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

docker run -it \
       -v $script_dir/.tezos-handson-client:/root/.tezos-handson-client \
       -v $script_dir/docker-node/scripts:/root/scripts \
       dailambda/tezos-handson:2019-08 bash
