# Tezos ハンズオン インストール

## Dockerのインストール

[Docker](https://www.docker.com) をインストールして、動作確認をしてください。

<dl>
<dt>Linux
<dd>Ubuntu ですと `apt install docker.io` でインストールできます。
`sudo` コマンドなしで `docker` を立ち上げられるようにすることをお勧めします。[詳しくは](https://docs.docker.com/install/linux/linux-postinstall/)。この設定をしなかった場合、ハンズオンで行う各コマンド呼び出しには`sudo`をつけなくてはいけなくなります。

<dt>Mac
<dd>[Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/install/)

<dt>Windows + 仮想 Linux 環境
<dd> Windows で一番簡単かつお勧めするのは、VMWare や VirtualPC に Ubuntu をインストールし、そこで Docker をインストールする方法です。演習は Ubuntu 内で行います。

<dt>Windows native
<dd>Windows 上での Docker を本当に良くわかっている人むけです。全くテストしていませんので動く保証はありません:

[Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/) もしくは
[Docker Toolbox on Windows](https://docs.docker.com/toolbox/toolbox_install_windows/) を使うことになるかと思います。

* `ligo`, `tezos-client`, `tezos-handson-node`, `test.sh` の各スクリプトを動かすには `bash` が必要です。
* また、これらのスクリプトはホスト側のディレクトリをいくつかコンテナ内にマウントしています。これを Windows でちゃんと行うには修正が必要だと思われます。

私は Docker Toolbox on Windows で試しましたが、成功していません。
</dl>

## イメージのインストール

ハンズオンでは二つのイメージを使いますので、**事前にインストールして**会場にお越しください。
会場のWiFiを使ってインストールも可能ですが、参加者の方の多くが同時にダウンロードを開始すると帯域を食いますし、時間もかかります。

次のコマンドで各イメージをダウンロードしてください:

```
$ docker pull dailambda/tezos-handson:2019-08 ↩️
$ docker pull dailambda/ligo:2019-08 ↩️
```

## Git repo のクローン

Git をインストールして、次のコマンドでハンズオンに使うファイルをダウンロードしてください:

```
$ git clone https://gitlab.com/dailambda/docker-tezos-hands-on --branch tezos-hands-on-2019-08 ↩️
```

`docker-tezos-hands-on` というディレクトリができます。実習はこのディレクトリ内で行います。

## 動作確認

`docker-tezos-hands-on`ディレクトリに`test.sh`というファイルがあるので、それを実行します。

```
$ cd docker-tezos-hands-on ↩️
$ ./test.sh ↩️
2019-08: Pulling from dailambda/tezos-handson
Digest: sha256:26..
Status: Image is up to date for dailambda/tezos-handson:2019-08
docker.io/dailambda/tezos-handson:2019-08
2019-08: Pulling from dailambda/ligo
Digest: sha256:64..
Status: Image is up to date for dailambda/ligo:2019-08
docker.io/dailambda/ligo:2019-08
Docker container ID=bb..
Waiting the node start-up...done!
Exiting
Shuting down container bb..
bb..
TEST SUCCEEDED.
```

最後に `TEST SUCCEEDED.` と出力されれば成功です。

`TEST FAILED.` もしくはそれ以外の場合は、 `test_log/` にログファイルが作成されますので、そこから頑張って問題を解決してください。

## TCP ポートについて

`tezos-handson-node` はホストマシンの TCP port 18732 を使います。
このポートを占有できるようにしてください。

どうしてもこのポートを使いたくない人は、 `.tezos-handson-node-port` というファイルにポート名を書いて置くとそれを使うはずです:

```
$ echo 20000 > .tezos-handson-node-port ↩️
$ ./tezos-handson-node
Using TEZOS_HANDSON_NODE_PORT=20000
...
```
