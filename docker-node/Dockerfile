FROM ubuntu:18.04
MAINTAINER Jun FURUSE <jun.furuse@dailambda.jp>

RUN apt-get clean

# Replacing the source

RUN sed -e 's|/archive\.ubuntu\.com/ubuntu|/ftp.jaist.ac.jp/pub/Linux/ubuntu|g' /etc/apt/sources.list > /tmp/sources.list
RUN mv /tmp/sources.list /etc/apt/sources.list
RUN apt-get update

# Basic tools

# netbase installs /etc/services, which is required, otheriwe we get:
#
# $ ./tezos-client -A 158.140.138.34 -P 12010 bootstrapped
# ./tezos-client -A 158.140.138.34 -P 12010 bootstrapped
# Warning:
#   Failed to acquire the protocol version from the node
#   Rpc request failed:
#      - meth: GET
#      - uri: http://158.140.138.34:12010/chains/main/blocks/head/metadata
#      - error: Unable to connect to the node: "resolution failed: unknown scheme"

RUN apt-get install -y --no-install-recommends curl git ca-certificates make patch sudo netbase jq

# OCaml and OPAM

RUN apt install -y --no-install-recommends pkg-config libgmp-dev libev-dev libhidapi-dev m4 unzip bubblewrap ocaml g++
USER root
WORKDIR /root
RUN git clone https://github.com/ocaml/opam
WORKDIR opam
RUN git checkout 2.0.3
RUN ./configure
RUN make lib-ext && make && make install
RUN opam init -a --bare --disable-sandboxing # bwrap does not work...

# Tezos

WORKDIR /root
RUN git clone https://gitlab.com/tezos/tezos
WORKDIR tezos

# Code: alphanet 2019-06-11
RUN git checkout 8015f6f24cd1bda8d533a838ace6857f115e8330

# Code: mainnet 2019-06-04
# RUN git checkout 20ce5a625781c6abbaaefdb9e7c8896aba799a7a

RUN make build-deps
RUN eval $(opam env) && env && make

WORKDIR /root

# Cleanup

WORKDIR /root
RUN du -sh /usr /home \
    && sudo apt remove -y ocaml g++ \
    && sudo apt autoremove -y \
    && sudo apt-get clean \
    && rm -rf /root/opam \
    && (cd /root/tezos; opam clean) \
    && du -sh /usr /home 

RUN du -sh /usr /home \
    && rm -rf /root/.opam \
    && rm -rf /root/.git \
    && rm -rf /root/_opam/.opam-switch \
    && rm -rf /root/_opam/.npm \
    && rm -rf /root/_opam/.ocp \
    && du -sh /usr /home 

# Scripts

RUN echo "export PATH=$HOME/scripts:$PATH" >> $HOME/.bashrc
RUN echo "export PATH=$HOME/scripts:$PATH" >> $HOME/.profile
RUN echo "export PS1='\u@DOCKER \w\$ '" >> $HOME/.bashrc
RUN echo "export PS1='\u@DOCKER \w\$ '" >> $HOME/.profile

ADD scripts /root/scripts

RUN /root/scripts/init-tezos-handson-node.sh

RUN du -sh /usr /home /root \
    && rm -rf /root/tezos/_build \
    && (tar zcf /tmp/tezos.tgz /root/tezos/_opam/lib/stublibs /root/tezos/_opam/lib/ocaml/stublibs /root/tezos/tezos-client /root/tezos/tezos-node /root/tezos/scripts /root/tezos/active_protocol_versions) \
    && rm -rf /root/tezos \
    && (cd /; tar zxvf /tmp/tezos.tgz) \
    && rm -rf /root/.opam \
    && rm -rf /root/.git \
    && rm -rf /root/_opam/.opam-switch \
    && rm -rf /root/_opam/.npm \
    && rm -rf /root/_opam/.ocp \
    && du -sh /usr /home /root

# That's all

# Surpress the warnings
ENV TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER Y

CMD [ "echo", "hello tezos" ]
