#! /usr/bin/env bash

set -e

local_node=/root/tezos/tezos-node

port=19732
rpc=18732
expected_pow=0.0
expected_connections=0
node_dir=/root/.template/tezos-handson-node
sandbox_file=$node_dir/sandbox.json
peers=("--no-bootstrap-peers")
peers+=("--private-mode")
node=/root/tezos/tezos-node
sandbox_param="--sandbox=$sandbox_file"

if [ ! -d $node_dir ]; then
    mkdir -p $node_dir
fi

if ! [ -f "${node_dir}/config.json" ]; then
    $node config init \
          --data-dir "$node_dir" \
          --net-addr "0.0.0.0:$port" \
          --rpc-addr "0.0.0.0:$rpc" \
          --expected-pow "$expected_pow" \
          --connections "$expected_connections"
fi

[ -f "${node_dir}/identity.json" ] || $node identity generate "$expected_pow" --data-dir "$node_dir"

if ! [ -f "$sandbox_file" ]; then
    cat > "$sandbox_file" <<EOF
{
    "genesis_pubkey":
      "edpkuSLWfVU1Vq7Jg9FucPyKmma6otcMHac9zG4oU1KMHSTBpJuGQ2"
}
EOF
fi

# $node run --data-dir "$node_dir" "${peers[@]}" "$sandbox_param" "$@" &
