#!/bin/bash

set -e

# Where the node is running
NODE_HOST=localhost

# Where compiled things are placed
HOME=/root
TEZOS_DIR=$HOME/tezos/

# Binaries
NODE_BIN=$TEZOS_DIR/tezos-node
CLIENT_BIN=$TEZOS_DIR/tezos-client

# Data directory is persistent.
# NODE_DATA_DIR will not be removed when tezos-handson-node.sh exits.
NODE_DATA_DIR=$HOME/.tezos-handson-node 
TEMPLATE_NODE_DATA_DIR=$HOME/.template/tezos-handson-node

# This is only for baking.  Not supposed for the hands-on participants
CLIENT_DATA_DIR=$NODE_DATA_DIR/baking-client

command="$NODE_BIN run --data-dir $NODE_DATA_DIR --no-bootstrap-peers --private-mode --sandbox $NODE_DATA_DIR/sandbox.json --connections=0"
echo Starting $command
$command &
PID=$!

if [ ! -d $CLIENT_DATA_DIR ]; then
    echo mkdir $CLIENT_DATA_DIR
    mkdir $CLIENT_DATA_DIR
fi

CLIENT="$CLIENT_BIN -A $NODE_HOST -P 18732 --base-dir $CLIENT_DATA_DIR"

if [ ! -f $NODE_DATA_DIR/version.json ]; then
    cp $TEMPLATE_NODE_DATA_DIR/* $NODE_DATA_DIR
    # Need to reset the baked block
    rm -f $CLIENT_DATA_DIR/blocks $CLIENT_DATA_DIR/nonces
fi

# # Wait the node starts running.
echo Waiting the node startup...
sleep 1
while true; do
    if $CLIENT rpc get /chains/main/blocks/head/metadata 2>&1; then
	echo Node is up.
	break
    else
	sleep 1
    fi
done

# expand_alias is required for the next eval
shopt -s expand_aliases
eval `$HOME/scripts/tezos-init-handson-client.sh 2`
 
# Protocol upgrade to Alpha

proto=`$CLIENT rpc get /chains/main/blocks/head/metadata | jq -r .protocol`
if [ "$proto" = "PrihK96nBAFSxVL1GLJTVhu9YnzkMFiBeuJRPA8NwuZVZCE1L6i" ]; then
    echo Activating Alpha protocol...
    tezos-activate-alpha
else
    echo Alpha protocol is already activated.
fi
 
# Start auto baker
BAKER=baker

CHECK_BAKE_PERIOD=${CHECK_BAKE_PERIOD:-15} # Check every 15 seconds
FORCE_BAKE_PERIOD=${FORCE_BAKE_PERIOD:-150} # Force to bake at least once per 5 mins
SECONDS=0 # auto variable increments every seconds

echo CHECK_BAKE_PERIOD=$CHECK_BAKE_PERIOD
echo FORCE_BAKE_PERIOD=$FORCE_BAKE_PERIOD

# echo addresses
# $CLIENT list known addresses
# echo addresses done

while true; do

    sleep $CHECK_BAKE_PERIOD
    
    MEMPOOL=`$CLIENT rpc get /chains/main/mempool/pending_operations | jq ".applied[]"`
    # echo Pool: `$CLIENT rpc get /chains/main/mempool/pending_operations`
    
    if [ -n "$MEMPOOL" ] || [ $SECONDS -ge $FORCE_BAKE_PERIOD ]; then
	# echo BAKE: $CLIENT bake for $BAKER
	if [ -n "$MEMPOOL" ]; then
	    echo Baker: Found operation: $MEMPOOL
	else
	    echo Baker: Time is up.  Force to build a block.
	fi
	$CLIENT --base-dir $CLIENT_DATA_DIR bake for $BAKER 2>&1 | sed -e 's/^/Baker: /'
	SECONDS=0 # auto variable increments every seconds
    fi
    
done
