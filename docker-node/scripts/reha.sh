#!/bin/bash

set -e

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"/../..
cd $script_dir

docker_down () {
    if [ -n "$CID" ]; then
	echo Shuting down container $CID
	docker container kill $CID
    fi
}

cleanup () {
    echo Exiting
    docker_down
    echo $FINAL
    exit 1
}

trap cleanup EXIT INT
 
if [ ! -d test_log ]; then
    mkdir test_log
fi

rm -rf .tezos-handson-node
mkdir .tezos-handson-node

rm -rf .tezos-handson-client
mkdir .tezos-handson-client

if [ -f .tezos-handson-node-port ]; then
    TEZOS_HANDSON_NODE_PORT=`cat .tezos-handson-node-port`
    echo Using TEZOS_HANDSON_NODE_PORT=$TEZOS_HANDSON_NODE_PORT
fi

# -it is required to kill by C-c

# app-armor prevents the container from being killed.
# we need --security-opt apparmor=unconfined

CID=`docker run --rm -d -it --security-opt apparmor=unconfined \
       -p ${TEZOS_HANDSON_NODE_PORT:-18732}:18732 \
       -v $script_dir/.tezos-handson-node:/root/.tezos-handson-node \
       -v $script_dir/.tezos-handson-client:/root/.tezos-handson-client \
       -v $script_dir/docker-node/scripts:/root/scripts \
       -e CHECK_BAKE_PERIOD=1 \
       dailambda/tezos-handson:2019-08 scripts/tezos-handson-node.sh`

echo "Docker container ID=$CID"

wait_for_the_node() {
    local count=0
    printf "Waiting the node start-up..."

    while true; do

	proto=`./tezos-client rpc get /chains/main/blocks/head/metadata | jq -r .protocol`
	if [ "$proto" != "PrihK96nBAFSxVL1GLJTVhu9YnzkMFiBeuJRPA8NwuZVZCE1L6i" ]; then
	    break
	fi

	count=$((count+1))
	if [ "$count" -ge 30 ]; then
	    echo " Timeout."
	    FINAL="TEST FAILED.  Check test_log/*.log"
	    exit 2
	fi
	printf "."
	sleep 1
    done
    echo 'done!'
}

wait_for_the_node

./tezos-client activate account alice with commitments/tz1MawerETND6bqJqx8GV3YHUrvMBCDasRBF.json
./tezos-client get balance for alice
./tezos-client get balance for `cat jun-account.txt`
./tezos-client add address jun `cat jun-account.txt`
./tezos-client get balance for jun
./tezos-client list known addresses
./tezos-client transfer 100 from alice to jun

echo alice2
./tezos-client gen keys alice2
./tezos-client transfer 10000 from alice to alice2 --burn-cap 0.257

echo first
./ligo compile-contract contracts/first.mligo main
./tezos-client originate contract first for alice transferring 0 from alice running contracts/first.tz --init '"initial value"' --burn-cap 0.506
./tezos-client list known contracts
./tezos-client get script code for first
./tezos-client get script storage for first
./tezos-client transfer 0 from alice to first --arg '"hello"'
./tezos-client get script storage for first
echo boomerang
./ligo compile-contract contracts/boomerang.mligo main
./tezos-client originate contract boomerang for alice transferring 0 from alice running contracts/boomerang.tz --burn-cap 0.745
./tezos-client transfer 10 from alice to boomerang

# In Ubuntu, files created by containers are owned by root.  Normal user cannot remove them.
# rm -rf .tezos-handson-node .tezos-handson-client test_log
FINAL="TEST SUCCEEDED."






