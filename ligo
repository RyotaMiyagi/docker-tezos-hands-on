#!/bin/sh

docker run --rm -it \
       -v `pwd`:/root/work \
       -w /root/work \
       -t dailambda/ligo:2019-08 \
       /root/ligo "$@"
