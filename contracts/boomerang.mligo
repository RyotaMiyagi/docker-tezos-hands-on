let main (param : unit) (storage: unit) =
  let ops = 
    if amount = 0tz then 
      ([] : operation list) 
    else 
      [ Operation.transaction () amount 
          (Operation.get_contract source : unit contract) ]
  in
  ( ops, () ) 
