let main (param : unit) (storage : unit) =
  let c = self in (* Proto alpha rejects this code due to "proto.alpha.selfInLambda" *)
  ( ([] : operation list), ())
