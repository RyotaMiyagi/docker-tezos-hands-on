(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let a = ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) in
  let c = (Operation.get_contract a : unit contract) in
  let op = Operation.transaction () 3tz c in
  ( [op], () )
    
