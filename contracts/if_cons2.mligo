(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let xs = [ 1 ; 2 ; 3 ] in
  let b = 
    match xs with
    | [] -> false
    | y::ys -> 
       (match ys with
       | [] -> false
       | z::zs -> 
          (match zs with
           | [] -> false
           | w::ws ->
              (match ws with
               | [] -> y + z + w = 6
               | _::_ -> false)))
  in
                            
  ( ([] : operation list), assert b )
    
