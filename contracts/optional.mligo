(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let x = Some 1 in
  let y = (None : int option) in

  let _ = assert (
      match x with
      | Some y -> y = 1
      | None -> false
    )
  in

  let _ = assert (
      match y with
      | None -> true
      | Some _ -> false
    )
  in
  
  ( ([] : operation list), () )
    
