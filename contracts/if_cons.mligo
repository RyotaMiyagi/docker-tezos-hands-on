(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let xs = [ true ; false ] in
  let b = 
    match xs with
    | [] -> false
    | y::ys -> y
  in
                            
  ( ([] : operation list), assert b )
    
