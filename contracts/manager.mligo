type param = 
  | Do of (unit -> operation list)
  | Default of unit

let main (param : param) (storage : key_hash) =
  match param with
  | Do f ->
     (* we cannot use failwith, let's use assert instead *)
     (* let _unit = if amount <> 0tz then failwith "haha" else () in *)
     let _unit = assert (amount = 0tz) in
     let c = implicit_account storage in
     let a = address c in
     let _unit = assert (sender = a) in 
     ( f (), storage )
  | Default _ ->
     ( ([] : operation list), storage )
                 
                 
