(* TESTABLE *)
type t = { name : string ; age : nat }

let main (param : unit) (storage : unit) =
  let x = { name = "Mike" ; age = 42p } in
  let _ = assert (x.name = "Mike") in
  let _ = assert (x.age = 42p) in
  ( ([] : operation list), ())    
