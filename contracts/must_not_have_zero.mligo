(*
let main (_storage : unit) (param : int list) =
  let _unit = List.iter param (fun (x : int) -> ()) in 
  ( ([] : operation list), () )
 *)

(* XXX iter does not work.
   XXX map and iter's argment order is flipped *)
let main (param : int list) (_storage : int list) =
  let xs = List.map param (fun (x : int) -> let _ = assert (x <> 0) in x) in
  ( ([] : operation list), () )
    
