(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let x = ( [] : string list ) in
  let x = [ 1; 2; 3 ] in
  let x = List.map x (fun (x:int) -> x + 1) in
  let _ = assert (List.length x = 3p) in
  let x = 0 :: x in
  let _ = assert (match x with
                  | [] -> false
                  | x::xs -> true) in
                            
  ( ([] : operation list), () )
    
