(* TESTABLE *)

type t = Foo of unit | Bar of int * int

let main (param : unit) (storage : unit) =
  let _ = assert (
      match Bar (1,1) with
      | Foo _ -> true
      | Bar i_i -> i_i.(0) + i_i.(1) = 2
    )
  in
  ( ([] : operation list), () )
    
