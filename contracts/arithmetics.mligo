(* TESTABLE *)
let main (param : unit) (storage : unit) =
  
  let _ = assert (3 + 4          = 7)     in
  let _ = assert (3p + 4p        = 7p)    in
  let _ = assert (1.2tz + 3.4tz  = 4.6tz) in
  let _ = assert (3 - 4          = -1)    in
  let _ = assert (3p - 4p        = -1)    in
  let _ = assert (10.2tz - 3.4tz = 6.8tz) in
  let _ = assert (3 * 4          = 12)    in
  let _ = assert (3p * 4p        = 12p)   in
  let _ = assert (3tz * 4p       = 12tz)  in
  let _ = assert (10 / 5         = 2)     in
  let _ = assert (10p / 5p       = 2p)    in
  let _ = assert (3tz / 2p       = 1.5tz) in
  let _ = assert (int 3p         = 3)     in
  let _ = assert (abs (-3)       = 3p)    in
  (* let x = - (3) in *) (* XXX unary minus is not supported *)

  ( ([] : operation list), () )
    
