(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let x = if 1 = 2 then 0tz else 10000tz in
  ( ([] : operation list), assert (x = 10000tz) )
    
