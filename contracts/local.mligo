(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let x = 1 in
  let quad = fun (x : int) -> x * 4 in (* XXX let quad (x : int) = x * 4  is considered "multiple patterns" *)
  let _ = assert (quad x = 4) in
  ( ([] : operation list), () )
