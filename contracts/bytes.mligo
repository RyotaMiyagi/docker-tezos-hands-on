(* TESTABLE *)
let main (param : unit) (storage : unit) =

  (* Bytes.pack can pack anything to bytes *)
  let bs : bytes = Bytes.pack (12, "hello", 3tz) in

  (* To recover the pack, use Bytes.unpack with a type annotation *)
  let vopt = (Bytes.unpack bs : (int * string * tez) option) in
  (* Failure is detected by `None` *)
  let v = match vopt with 
    | None -> let _ = assert false in (0, "dummy", 0tz) (* No easy way to fail *)
    | Some v -> v
  in
  
  (* Recovered value must be equal to the original *)
  let _ = assert (v.(0) = 12) in
  let _ = assert (v.(1) = "hello") in
  let _ = assert (v.(2) = 3tz) in

  (* length *)
  let l = Bytes.length bs in
  
  (* concatenation *)
  let bs' = Bytes.concat bs bs in
  
  (* subbytes *)
  let bs'' = Bytes.sub 0p l bs' in

  (* comparison *)
  let _ = assert (bs = bs'') in

  ( ([] : operation list), () )
