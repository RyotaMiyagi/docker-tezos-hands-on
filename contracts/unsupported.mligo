(* TESTABLE *)
let main (param : unit) (storage : unit) =

  (* string *)
  (* let x = "你好" in *) (* XXX UTF-8 is not working *)

  let x6 = () in 
  (* let _ = assert ( x6 = () ) in *) (* XXX unit is not comparable.  This is a "feature" of michelson *)
  
  (* bool *)
  let x7 = true in 
  (* let _ = assert (x7 = true) in *) (* XXX bools are not comparable. *)
  
  (* XXX timestamp *)

  (* let x = - (3) in *) (* XXX unary minus is not supported *)

  ( ([] : operation list), ())
