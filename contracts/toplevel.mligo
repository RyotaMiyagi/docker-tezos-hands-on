(* TESTABLE *)
let x = 1

let quad (x : int) = x * 4

let _ = assert true
      
let main (param : unit) (storage : unit) =
  let _ = assert (quad x = 4) in
  ( ([] : operation list), ())
    
