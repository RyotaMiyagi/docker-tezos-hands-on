(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let m = (Map [ ] : (string, int) map) in
  let m = Map [ ("Alice", 1) ; ("Bob", 2) ; ("Charlie", 3) ] in
  let _ = assert (match Map.find_opt "Alice" m with
                  | None -> false
                  | Some v -> v = 1)
  in
  let _ = assert (Map.find "Bob" m = 2) in
  let m = Map.add "Daniel" 4 m in
  let m = Map.add "Alice" 3 m in (* add can override existing bindings *)
  let m = Map.update "Alice" (Some 1) m in
  let m = Map.update "Ethan" (Some 5) m in (* insertion by update *)
  (* Map.update "Alice" None m   fails due to 'syntax error'.  Needs a type annotation *)
  let m = Map.update "Alice" (None : int option) m in (* removal by update *)
  let m = Map.remove "Ethan" m in

  let _ = assert (match Map.find_opt "Alice" m with None -> true | Some _ -> false) in
  let _ = assert (match Map.find_opt "Daniel" m with None -> false | Some x -> x = 4) in
  let _ = assert (match Map.find_opt "Ethan" m with None -> true | Some _ -> false) in

  ( ([] : operation list), () )

