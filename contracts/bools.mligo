(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let x1 = true && false in
  let x2 = true || false in
  let x3 = not true in

  ( ([] : operation list), assert (not x1 && x2 && not x3) )
    
