(* TESTABLE *)
let main (param : unit) (storage : unit) =

  (* string *)
  let x1 = "string" in
  (* let x = "你好" in *) (* UTF-8 is not working *)

  (* int *)
  let x2 = 123 in
  let x3 = -32 in
  
  (* nat *)
  let x4 = 34p in
  
  (* tez *)
  let x5 = 1.25tz in
  
  (* unit *)
  let x6 = () in
  
  (* bool *)
  let x7 = true in
  let x8 = false in
  
  (* address *)
  let x9 = ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) in
  let x10 = ("KT1JepfBfMSqkQyf9B1ndvURghGsSB8YCLMD" : address) in

  ( ([] : operation list),

    assert ( 
      x1 = "string" 
      && x2 = 123 
      && x3 = -32 
      && x4 = 34p 
      && x5 = 1.25tz  
      )
  )
