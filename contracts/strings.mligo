(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let s = "hello" in
  let _ = assert (String.length s = 5p) in
  let _ = assert (String.sub 0p 4p s = "hell") in
  let _ = assert (String.sub 1p 4p s = "ello") in
  let _ = assert (String.concat s " world" = "hello world") in

  ( ([] : operation list), () )
    
