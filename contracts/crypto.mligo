(* TESTABLE *)
let main (param : unit) (storage : unit) =
  (* let x = Crypto.hash 12 in *) (* unrecognized constant HASH *)
  (* let x = Crypto.blake2b "hello" in  *) (* unrecognized constant BLAKE2B *)
  
  (* let x = ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : hash_key) in *)

  let x : bytes = Crypto.sha256 (Bytes.pack "hello") in
  let x : bytes = Crypto.sha512 (Bytes.pack "hello") in
  (* let x = Crypto.hash_key (Bytes.pack "hello") in (* not a key *) *)
  (* let x = Crypto.check (Bytes.pack "hello") (Bytes.pack "hello") (Bytes.pack "hello") in *) (* not a key *)

  ( ([] : operation list), () )
    
