#!/bin/bash

set -e

docker_down () {
    if [ -n "$CID" ]; then
	echo Shuting down container $CID
	docker logs $CID > test_log/node.log
	docker container kill $CID
    fi
}

cleanup () {
    echo Exiting
    docker_down
    echo $FINAL
    exit 1
}

trap cleanup EXIT INT
 
docker pull dailambda/tezos-handson:2019-08
docker pull dailambda/ligo:2019-08

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

cd $script_dir

if [ ! -d test_log ]; then
    mkdir test_log
fi

rm -f test_log/client.log
rm -f test_log/node.log

if [ -f .tezos-handson-node-port ]; then
    TEZOS_HANDSON_NODE_PORT=`cat .tezos-handson-node-port`
    echo Using TEZOS_HANDSON_NODE_PORT=$TEZOS_HANDSON_NODE_PORT
fi

if [ ! -d .tezos-handson-node-test ]; then
    mkdir .tezos-handson-node-test
fi
if [ ! -d .tezos-handson-client-test ]; then
    mkdir .tezos-handson-client-test
fi

# -it is required to kill by C-c

# app-armor prevents the container from being killed.
# we need --security-opt apparmor=unconfined

CID=`docker run --rm -d -it --security-opt apparmor=unconfined \
       -p ${TEZOS_HANDSON_NODE_PORT:-18732}:18732 \
       -v $script_dir/.tezos-handson-node-test:/root/.tezos-handson-node \
       -v $script_dir/.tezos-handson-client-test:/root/.tezos-handson-client \
       -v $script_dir/docker-node/scripts:/root/scripts \
       dailambda/tezos-handson:2019-08 scripts/tezos-handson-node.sh`

echo "Docker container ID=$CID"

wait_for_the_node() {
    local count=0
    printf "Waiting the node start-up..."
    while ! ./tezos-client rpc get /chains/main/blocks/head/hash >> test_log/client.log 2>&1
    do
	count=$((count+1))
	if [ "$count" -ge 30 ]; then
	    echo " Timeout."
	    FINAL="TEST FAILED.  Check test_log/*.log"
	    exit 2
	fi
	printf "."
	sleep 1
    done
    echo 'done!'
}

wait_for_the_node

# # Let docker remove the files created
# docker run --rm -d -it --security-opt apparmor=unconfined \
#        -v $script_dir/.tezos-handson-node-test:/root/.tezos-handson-node \
#        -v $script_dir/.tezos-handson-client-test:/root/.tezos-handson-client \
#        dailambda/tezos-handson:2019-08 rm -rf /root/.tezos-handson-node /root/.tezos-hands-client
# # rm -rf .tezos-handson-node-test .tezos-handson-client-test test_log
FINAL="TEST SUCCEEDED."


