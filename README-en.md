# Tezos Hands-on Kit Install

Slides: https://dailambda.gitlab.io/slides-html/2019-08-29-taipei/

## Support Slack channel

You can ask questions of the kit and hands-on here in this Slack channel:

https://join.slack.com/t/tezos-handson/shared_invite/enQtNzM0NjcyOTQ4MDE2LTlhNzU1YzgwY2UwZWViZjA4YWYzNmY1YTczYjVlNmJmNDcyMzMxMmYzNDgzZjMwNWYxYTlkYzMxMzUzNDU1MzY

The channel is also used for other Tezos hands-ons.
No need to worry about the past posts.  You can use English, Chinese or Japanese.

## Docker installation

Install [Docker](https://www.docker.com) and check it works.

Check out the official [detailed instructions](https://docs.docker.com/).
Here are some additional notes from us:

<dl>
<dt>Mac
<dd>The easiest. [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/install/)

<dt>Linux
<dd>In Ubuntu (and some other `apt` based distributions), `apt install docker.io` simply install Docker.  It may be bit older than the latest version, but it should work.
<dd>I recommend you configure `docker` so that you can launch it without `sudo` command. [Details](https://docs.docker.com/install/linux/linux-postinstall/).  If you do not, you will have to use `sudo` for every commands used in the hands-on.

<dt>Windows + Virtual Linux
<dd>The only easiest, verified, and recommended way in Windows is to install Ubuntu over VMWare or VirtualPC, then install Docker over Ubuntu.  You will launch commands from Ubuntu during the hands-on.
<dd>Never try Linux over WSL.

<dt>Windows native
<dd>Only if you use Docker on Windows already.  Not tested at all by us.

Install [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/) 
or [Docker Toolbox on Windows](https://docs.docker.com/toolbox/toolbox_install_windows/).

The scripts `ligo`, `tezos-client`, `tezos-handson-node`, and `test.sh` use bash.  You have to find a way to use it.
</dl>

## Docker image installation

We use 2 images in Hands-on.  **Please install them before the event** with a good network connection.
You may download images (500MB) at the venue, but the network bandwidth is usually limited and it takes time.

You can download the images using the following commands:

```
$ docker pull dailambda/tezos-handson:2019-08 ↩️
$ docker pull dailambda/ligo:2019-08 ↩️
```

## Git repo clone

Install Git and clone the repository with the hands-on materials:

```
$ git clone https://gitlab.com/dailambda/docker-tezos-hands-on --branch tezos-hands-on-2019-08 ↩️
```

It should create a directory `docker-tezos-hands-on`.
You should work in this directory while the hands-on.

## Check it works

There is a script `test.sh` under `docker-tezos-hands-on`.  Execute it:

```
$ cd docker-tezos-hands-on ↩️
$ ./test.sh ↩️
2019-08: Pulling from dailambda/tezos-handson
Digest: sha256:26..
Status: Image is up to date for dailambda/tezos-handson:2019-08
docker.io/dailambda/tezos-handson:2019-08
2019-08: Pulling from dailambda/ligo
Digest: sha256:64..
Status: Image is up to date for dailambda/ligo:2019-08
docker.io/dailambda/ligo:2019-08
Docker container ID=bb..
Waiting the node start-up...done!
Exiting
Shuting down container bb..
bb..
TEST SUCCEEDED.
```

If you see `TEST SUCCEEDED.`, congrats, you are ready for the hands-on.

Otherwise, if you see `TEST FAILED.` or else, `test_log/` directory should have log files.  Read them and fix the problem...

## TCP port

`tezos-handson-node` command binds TCP port 18732 of the host-machine.
You should free this port for the hands-on.

If the port is already occupied, you can set environment variable `TEZOS_HANDSON_NODE_PORT` to override it:

```
$ export TEZOS_HANDSON_NODE_PORT 20000 ↩️
$ ./tezos-handson-node
...
```

```
$ export TEZOS_HANDSON_NODE_PORT 20000 ↩️
$ ./tezos-client ...
...
```
